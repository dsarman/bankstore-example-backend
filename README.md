# BankStore Example app backend

Example Spring backend application.

Core technologies:
- Spring WebFlux
- Redis and its reactive client
- JPA

The app expects a started Redis server.
In local development, database is handled using in memory H2.
In production the setup is prepared for MSSQL database.

Note:
The main purpose of this app was to try and explore the WebFlux server.
In reality, you should probably not roll your own cache like here, but use the Redis instance as a cache backend using traditional Spring `@Cacheable` and other annotations.

TODO:
- [ ] Add modified timestamps to DB entities to allow fetching of only changed entities (or use GraphQL).
- [ ] Replace the in-memory Spring Security user repository by a DB backed real one.
- [ ] Replace Basic Auth with something better, preferably Google Sign-in and OAuth2.
- [ ] More tests, right now only "happy" path is covered.
- [ ] Many more improvements.
 