package com.example.bankstorebackend;

import com.example.bankstorebackend.data.dto.BankDTO;
import com.example.bankstorebackend.data.model.Bank;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@SpringBootApplication
public class BankstorebackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankstorebackendApplication.class, args);
    }

    @Bean
    @Qualifier("jdbcScheduler")
    public Scheduler getJdbcScheduler() {
        return Schedulers.elastic();
    }

    @Bean
    public ModelMapper getModelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        PropertyMap<BankDTO, Bank> propertyMap = new PropertyMap<BankDTO, Bank>() {
            @Override
            protected void configure() {
                map().setId(null);
                map(source.getUserId()).setUserId(source.getUserId());
                map(source.getAppId()).setAppId(source.getAppId());
            }
        };
        modelMapper.addMappings(propertyMap);
        return modelMapper;
    }
}
