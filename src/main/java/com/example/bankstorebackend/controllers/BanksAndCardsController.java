package com.example.bankstorebackend.controllers;

import com.example.bankstorebackend.data.dto.BankDTO;
import com.example.bankstorebackend.exceptions.BankNotFoundException;
import com.example.bankstorebackend.services.BanksAndCardsService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.security.Principal;

@RestController
@AllArgsConstructor
public class BanksAndCardsController {

    private final BanksAndCardsService banksAndCardsService;

    @GetMapping("/list")
    public Flux<BankDTO> list(Mono<Principal> principal) {
        return principal.flatMapMany(user -> banksAndCardsService.findByUserId(user.getName()));
    }

    @PostMapping(value = "/save", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Boolean> save(@RequestBody BankDTO bankDTO, Mono<Principal> principal) {
        return principal.flatMap(user -> {
            bankDTO.setUserId(user.getName());
            return banksAndCardsService.save(bankDTO);
        });
    }

    @GetMapping(value = "/detail/{appId}")
    public Mono<BankDTO> detail(@PathVariable("appId") long appId, Mono<Principal> principal) {
        return principal
                .flatMap(user -> banksAndCardsService.findByUserIdAndBankAppId(user.getName(), appId))
                .onErrorResume(e -> Mono.error(new BankNotFoundException(e)));
    }

    @DeleteMapping("/remove/{appId}")
    public Mono<Boolean> remove(@PathVariable("appId") long appId, Mono<Principal> principal) {
        return principal
                .flatMap(user -> banksAndCardsService.deleteByUserIdAndBankAppId(user.getName(), appId))
                .onErrorResume(e -> Mono.error(new BankNotFoundException(e)));
    }
}
