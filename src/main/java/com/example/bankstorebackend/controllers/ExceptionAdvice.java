package com.example.bankstorebackend.controllers;

import com.example.bankstorebackend.exceptions.BankCouldNotBeDeletedException;
import com.example.bankstorebackend.exceptions.BankCouldNotBeSavedException;
import com.example.bankstorebackend.exceptions.BankNotFoundException;
import com.example.bankstorebackend.exceptions.RedisOperationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ExceptionAdvice {
    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<String> handleRuntimeException(RuntimeException e) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    @ExceptionHandler({RedisOperationException.class})
    public ResponseEntity<String> handleRedisOperationException(RedisOperationException e) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    @ExceptionHandler({BankNotFoundException.class})
    public ResponseEntity<String> handleNotFoundException(BankNotFoundException e) {
        return error(HttpStatus.NOT_FOUND, e);
    }

    @ExceptionHandler({BankCouldNotBeSavedException.class, BankCouldNotBeDeletedException.class})
    public ResponseEntity<String> handleDataManipulationExceptions(Exception e) {
        return error(HttpStatus.BAD_REQUEST, e);
    }

    private ResponseEntity<String> error(HttpStatus status, Exception e) {
        log.error("Exception occurred in endpoint. ", e);
        return ResponseEntity.status(status).build();
    }
}
