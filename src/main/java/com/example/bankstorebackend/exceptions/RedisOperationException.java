package com.example.bankstorebackend.exceptions;

public class RedisOperationException extends Exception {
    public RedisOperationException(String message) {
        super(message);
    }
}
