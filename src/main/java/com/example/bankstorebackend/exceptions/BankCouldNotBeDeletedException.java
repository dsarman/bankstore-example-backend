package com.example.bankstorebackend.exceptions;

public class BankCouldNotBeDeletedException extends Exception {
    public BankCouldNotBeDeletedException(Throwable cause) {
        super(cause);
    }
}
