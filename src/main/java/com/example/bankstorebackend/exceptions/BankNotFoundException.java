package com.example.bankstorebackend.exceptions;

public class BankNotFoundException extends Exception {
    public BankNotFoundException() {
        super();
    }

    public BankNotFoundException(Throwable cause) {
        super(cause);
    }
}
