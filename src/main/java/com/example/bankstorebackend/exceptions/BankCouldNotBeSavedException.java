package com.example.bankstorebackend.exceptions;

public class BankCouldNotBeSavedException extends Exception{
    public BankCouldNotBeSavedException(Throwable cause) {
        super(cause);
    }
}
