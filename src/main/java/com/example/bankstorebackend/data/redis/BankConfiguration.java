package com.example.bankstorebackend.data.redis;

import com.example.bankstorebackend.data.dto.BankDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class BankConfiguration {
    @Bean
    ReactiveRedisOperations<String, BankDTO> redisOperations(ReactiveRedisConnectionFactory factory) {
        Jackson2JsonRedisSerializer<BankDTO> serializer = new Jackson2JsonRedisSerializer<>(BankDTO.class);
        serializer.setObjectMapper(new ObjectMapper().registerModule(new JavaTimeModule()));

        RedisSerializationContext.RedisSerializationContextBuilder<String, BankDTO> builder =
                RedisSerializationContext.newSerializationContext(new StringRedisSerializer());

        RedisSerializationContext<String, BankDTO> context = builder.value(serializer).build();

        return new ReactiveRedisTemplate<>(factory, context);
    }
}
