package com.example.bankstorebackend.data.repositories;

import com.example.bankstorebackend.data.model.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface CardRepository extends JpaRepository<Card, Long> {
    @Transactional
    @Modifying
    @Query("DELETE FROM Card c WHERE c IN (" +
            "SELECT c FROM Bank b JOIN b.cards c WHERE b.userId = :userId AND b.appId = :bankAppId)")
    void deleteByUserIdAndBankAppId(String userId, Long bankAppId);
}
