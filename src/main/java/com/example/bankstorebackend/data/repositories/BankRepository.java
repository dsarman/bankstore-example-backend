package com.example.bankstorebackend.data.repositories;

import com.example.bankstorebackend.data.model.Bank;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BankRepository extends JpaRepository<Bank, Long> {
    List<Bank> findByUserId(String userId);

    Optional<Bank> findByUserIdAndAppId(String userId, long appId);

    void deleteByUserIdAndAppId(String userId, long appId);
}
