package com.example.bankstorebackend.data.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.YearMonth;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Card {
    @Id
    @GeneratedValue
    @Column(name = "CARD_ID")
    private Long id;

    private Long appId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "BANK_ID")
    private Bank bank;

    private String name;
    private Long number;
    private YearMonth expiration;
    private Integer ccv;
}
