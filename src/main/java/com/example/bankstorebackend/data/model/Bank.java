package com.example.bankstorebackend.data.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Bank {
    @Id
    @GeneratedValue
    @Column(name = "BANK_ID")
    private Long id;

    @ToString.Exclude
    @OneToMany(mappedBy = "bank", fetch = FetchType.EAGER)
    @Builder.Default
    private List<Card> cards = new ArrayList<>();

    private Long appId;
    private String userId;
    private String name;
}
