package com.example.bankstorebackend.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BankDTO {
    private Long appId;
    private String userId;
    private String name;
    private List<CardDTO> cards = new ArrayList<>();
}
