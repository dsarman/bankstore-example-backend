package com.example.bankstorebackend.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.YearMonth;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CardDTO {
    private Long appId;
    private String name;
    private Long number;
    private YearMonth expiration;
    private Integer ccv;
}
