package com.example.bankstorebackend.services;

import com.example.bankstorebackend.data.dto.BankDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface BanksAndCardsService {
    /**
     * Finds all Banks with given user id.
     *
     * @param userId User id of wanted banks.
     */
    Flux<BankDTO> findByUserId(String userId);

    /**
     * Saves bank and cards contained in the DTO.
     *
     * @return true if the value was saved, false otherwise.
     */
    Mono<Boolean> save(BankDTO bankDTO);

    /**
     * Finds a bank and its cards for given user and bank app id.
     */
    Mono<BankDTO> findByUserIdAndBankAppId(String userId, long bankAppId);

    /**
     * Deletes a bank and its cards for given user and bank id.
     */
    Mono<Boolean> deleteByUserIdAndBankAppId(String userId, long bankAppId);
}
