package com.example.bankstorebackend.services;

import com.example.bankstorebackend.data.dto.BankDTO;
import com.example.bankstorebackend.data.model.Bank;
import com.example.bankstorebackend.data.model.Card;
import com.example.bankstorebackend.data.repositories.BankRepository;
import com.example.bankstorebackend.data.repositories.CardRepository;
import com.example.bankstorebackend.exceptions.BankCouldNotBeDeletedException;
import com.example.bankstorebackend.exceptions.BankCouldNotBeSavedException;
import com.example.bankstorebackend.exceptions.BankNotFoundException;
import com.example.bankstorebackend.exceptions.RedisOperationException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BanksAndCardsServiceImpl implements BanksAndCardsService {
    private final static int REDIS_TTL_MINUTES = 30;
    private final BankRepository bankRepository;
    private final CardRepository cardRepository;
    private final ReactiveRedisOperations<String, BankDTO> bankOps;
    private final Scheduler jdbcScheduler;
    private final ModelMapper modelMapper;
    private final TransactionTemplate transactionTemplate;

    public BanksAndCardsServiceImpl(BankRepository bankRepository,
                                    CardRepository cardRepository,
                                    ReactiveRedisOperations<String, BankDTO> bankOps,
                                    ModelMapper modelMapper,
                                    @Qualifier("jdbcScheduler") Scheduler jdbcScheduler,
                                    PlatformTransactionManager transactionManager
    ) {
        this.bankRepository = bankRepository;
        this.cardRepository = cardRepository;
        this.bankOps = bankOps;
        this.jdbcScheduler = jdbcScheduler;
        this.modelMapper = modelMapper;
        this.transactionTemplate = new TransactionTemplate(transactionManager);
    }


    @Override
    public Mono<Boolean> save(BankDTO bankDTO) {
        // First we save the value into Redis
        return saveToRedis(bankDTO)
                // Then, if successful, we save the value into database
                .flatMap(success -> {
                    if (!success) {
                        String errorMessage = "Setting all existing bank DTOs in Redis under userId " + bankDTO.getUserId() + "to expire failed";
                        return Mono.error(new RedisOperationException(errorMessage));
                    }
                    return saveInDB(bankDTO).map(Objects::nonNull);
                });
    }

    /**
     * Saves the Bank and its Cards into database in one transaction.
     *
     * @param bankDTO Bank DTO object containing the data tha will be saved.
     * @return Mono with boolean signifying the operations success.
     */
    private Mono<BankDTO> saveInDB(BankDTO bankDTO) {
        Mono<BankDTO> defer = Mono.defer(() -> {
            return transactionTemplate.execute(status -> {
                try {
                    // Find existing bank if we are updating
                    Optional<Bank> existingBank = bankRepository.findByUserIdAndAppId(
                            bankDTO.getUserId(), bankDTO.getAppId());
                    Bank bank = existingBank.orElseGet(() -> Bank.builder()
                            .appId(bankDTO.getAppId())
                            .userId(bankDTO.getUserId())
                            .build());
                    bank.setName(bankDTO.getName());
                    Bank savedBank = bankRepository.save(bank);

                    List<Card> cards = bankDTO.getCards()
                            .stream()
                            .map(cardDTO -> {
                                Card card = modelMapper.map(cardDTO, Card.class);
                                card.setBank(savedBank);
                                savedBank.getCards().add(card);
                                return card;
                            })
                            .collect(Collectors.toList());

                    //TODO: refactor to actually update cards, not this delete and save all approach.
                    cardRepository.deleteByUserIdAndBankAppId(bankDTO.getUserId(), bankDTO.getAppId());
                    cardRepository.saveAll(cards);

                    return Mono.just(bankDTO);
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    return Mono.error(new BankCouldNotBeSavedException(e));
                }
            });
        });
        return defer.subscribeOn(jdbcScheduler);
    }

    @Override
    public Mono<BankDTO> findByUserIdAndBankAppId(String userId, long bankAppId) {
        return bankOps.opsForValue().get(userId + "-" + bankAppId)
                .switchIfEmpty(findByUserIdAndBankAppIdInDB(userId, bankAppId));
    }

    /**
     * Finds Bank for given user and bank id in database and returns it.
     */
    private Mono<BankDTO> findByUserIdAndBankAppIdInDB(String userId, long bankAppId) {
        Mono<BankDTO> defer = Mono.defer(() -> {
            Optional<Bank> bank = bankRepository.findByUserIdAndAppId(userId, bankAppId);
            if (bank.isPresent()) {
                BankDTO bankDTO = modelMapper.map(bank.get(), BankDTO.class);

                // We do this to fill the cache with all banks for given user.
                return finByUserIdInDb(userId)
                        .reduce((a, b) -> bankDTO);
            }
            return Mono.error(new BankNotFoundException());
        });
        return defer.subscribeOn(jdbcScheduler);
    }

    @Override
    public Mono<Boolean> deleteByUserIdAndBankAppId(String userId, long bankAppId) {
        return bankOps.opsForValue().delete(userId + "-" + bankAppId)
                .flatMap(success -> deleteByUserIdAndBankAppIdInDB(userId, bankAppId));
    }


    /**
     * Deletes bank and its associated cards from the database.
     *
     * @return success.
     */
    private Mono<Boolean> deleteByUserIdAndBankAppIdInDB(String userId, long bankAppId) {
        Mono<Boolean> defer = Mono.defer(() -> transactionTemplate.execute(status -> {
            try {
                cardRepository.deleteByUserIdAndBankAppId(userId, bankAppId);
                bankRepository.deleteByUserIdAndAppId(userId, bankAppId);
                return Mono.just(true);
            } catch (DataAccessException e) {
                status.setRollbackOnly();
                return Mono.error(new BankCouldNotBeDeletedException(e));
            }
        }));
        return defer.subscribeOn(jdbcScheduler);
    }

    /**
     * Finds all banks stored under the given user id.
     * Looks into Redis first, only if that comes up empty we search the database.
     *
     * @param userId User id of wanted banks.
     * @return Flux of all found banks.
     */
    @Override
    public Flux<BankDTO> findByUserId(String userId) {
        Flux<BankDTO> banksFromCache = getKeysForUserId(userId)
                .flatMap(bankOps.opsForValue()::get);
        return banksFromCache
                .hasElements()
                .flatMapMany(hasElements -> {
                    if (hasElements) {
                        return banksFromCache;
                    } else {
                        return finByUserIdInDb(userId);
                    }
                });
    }

    /**
     * Finds Banks with given user id in database.
     * Also fills the Redis "cache" with the found values.
     *
     * @param userId user id of wanted banks.
     */
    private Flux<BankDTO> finByUserIdInDb(String userId) {
        Instant expiration = getInstantMinutesInFuture();
        Flux<BankDTO> defer = Flux.defer(() -> {
            List<Bank> banks = bankRepository.findByUserId(userId);
            List<BankDTO> bankDTOs = banks
                    .stream()
                    .map(bank -> {
                        BankDTO bankDTO = modelMapper.map(bank, BankDTO.class);
                        // We insert the value into Redis, so we do not have to reach to the
                        // database if the request is repeated after a while
                        String bankKey = getBankKey(bankDTO);
                        bankOps.opsForValue().set(bankKey, bankDTO);
                        bankOps.expireAt(bankKey, expiration);
                        return bankDTO;
                    })
                    .collect(Collectors.toList());
            return Flux.fromIterable(bankDTOs);
        });
        return defer.subscribeOn(jdbcScheduler);
    }

    /**
     * Saves given value into Redis.
     * Also sets the same expiration for all values for given user.
     *
     * @param bankDTO Bank object to be saved.
     * @return true if successfully saved, false otherwise
     */
    private Mono<Boolean> saveToRedis(BankDTO bankDTO) {
        return bankOps.opsForValue().set(getBankKey(bankDTO), bankDTO)
                .flatMapMany(success -> {
                    if (!success) {
                        String errorMessage = "Could not insert bank dto " + bankDTO + " into Redis";
                        return Flux.error(new RedisOperationException(errorMessage));
                    }
                    return setRedisExpirationForUserId(bankDTO.getUserId());
                })
                .reduce((a, b) -> a && b);
    }

    /**
     * Sets expiration for all bankDTOs of the user to the same time.
     */
    private Flux<Boolean> setRedisExpirationForUserId(String userId) {
        //Set expiration for all of these users cards to the same time.
        Instant expiration = getInstantMinutesInFuture();
        return getKeysForUserId(userId)
                .flatMap(bankKey -> bankOps.expireAt(bankKey, expiration));
    }

    /**
     * Returns the string representation used as a key in Redis for the given bank DTO object.
     */
    private String getBankKey(BankDTO bankDTO) {
        return bankDTO.getUserId() + "-" + bankDTO.getAppId();
    }

    private Flux<String> getKeysForUserId(String userId) {
        ScanOptions scanOptions = ScanOptions.scanOptions().match(userId + "*").build();
        return bankOps.scan(scanOptions);
    }

    private Instant getInstantMinutesInFuture() {
        return LocalDateTime
                .now()
                .plusMinutes(BanksAndCardsServiceImpl.REDIS_TTL_MINUTES)
                .atZone(ZoneId.systemDefault()).toInstant();
    }
}
