package com.example.bankstorebackend;

import com.example.bankstorebackend.data.dto.BankDTO;
import com.example.bankstorebackend.data.dto.CardDTO;
import com.example.bankstorebackend.data.model.Bank;
import com.example.bankstorebackend.data.repositories.BankRepository;
import com.example.bankstorebackend.data.repositories.CardRepository;
import com.github.javafaker.Faker;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.util.Base64Utils;

import java.time.YearMonth;
import java.util.ArrayList;

import static java.nio.charset.StandardCharsets.UTF_8;

public class TestUtils {
    private static Faker faker = new Faker();

    public static CardDTO createCardDTO() {
        CardDTO cardDTO = new CardDTO();
        cardDTO.setAppId(faker.random().nextLong());
        cardDTO.setCcv(faker.random().nextInt(100, 999));
        cardDTO.setExpiration(YearMonth.of(faker.random().nextInt(9999), faker.random().nextInt(1, 12)));
        cardDTO.setName(faker.rickAndMorty().character());
        cardDTO.setNumber(faker.random().nextLong());

        return cardDTO;
    }

    public static Bank createBank() {
        Bank bank = new Bank();
        bank.setAppId(faker.random().nextLong());
        bank.setUserId(faker.internet().uuid());
        bank.setName(faker.rickAndMorty().location());
        return bank;
    }

    public static BankDTO createBankDTO() {
        Bank bank = createBank();
        return new BankDTO(bank.getAppId(), bank.getUserId(), bank.getName(), new ArrayList<>());
    }


    public static String getAuthHeader(String username, String password) {
        return "Basic " + Base64Utils
                .encodeToString((username + ":" + password).getBytes(UTF_8));
    }

    public static void cleanAllPersistedData(CardRepository cardRepository,
                                             BankRepository bankRepository,
                                             ReactiveRedisOperations<String, BankDTO> bankOps) {
        cardRepository.deleteAll();
        bankRepository.deleteAll();
        bankOps.keys("*").flatMap(bankOps::delete).blockLast();
    }
}
