package com.example.bankstorebackend.controllers;

import com.example.bankstorebackend.TestUtils;
import com.example.bankstorebackend.data.dto.BankDTO;
import com.example.bankstorebackend.data.dto.CardDTO;
import com.example.bankstorebackend.data.model.Bank;
import com.example.bankstorebackend.data.repositories.BankRepository;
import com.example.bankstorebackend.data.repositories.CardRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureWebTestClient
public class BanksAndCardsControllerTest {
    @Autowired
    private BankRepository bankRepository;
    @Autowired
    private CardRepository cardRepository;
    @Autowired
    private WebTestClient webTestClient;
    @Autowired
    private ReactiveRedisOperations<String, BankDTO> bankOps;

    @Before
    public void setUp() throws Exception {
        TestUtils.cleanAllPersistedData(cardRepository, bankRepository, bankOps);
    }

    @Test
    public void testUnauthorizedList() {
        webTestClient.get()
                .uri("/list")
                .exchange()
                .expectStatus()
                .isUnauthorized();
    }

    @Test
    public void testList() {
        Bank bank1 = TestUtils.createBank();
        bank1.setUserId("user1");

        Bank bank2 = TestUtils.createBank();
        bank2.setUserId("user2");

        bankRepository.saveAll(Arrays.asList(bank1, bank2));

        webTestClient.get().uri("/list")
                .header("Authorization", TestUtils.getAuthHeader("user1", "user1"))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(new ParameterizedTypeReference<List<BankDTO>>() {
                })
                .value(bankDTOS -> {
                    assertEquals(1, bankDTOS.size());
                    BankDTO bankDTO = bankDTOS.get(0);
                    assertEquals(bank1.getAppId(), bankDTO.getAppId());
                    assertEquals(bank1.getUserId(), bankDTO.getUserId());
                    assertEquals(bank1.getName(), bankDTO.getName());
                });
    }

    @Test
    public void testUnauthorizedSave() {
        BankDTO bankDTO = TestUtils.createBankDTO();

        webTestClient.post()
                .uri("/save")
                .body(fromObject(bankDTO))
                .exchange()
                .expectStatus()
                .isUnauthorized();
    }

    @Test
    public void testSave() {
        BankDTO bankDTO = TestUtils.createBankDTO();
        CardDTO cardDTO = TestUtils.createCardDTO();
        bankDTO.getCards().add(cardDTO);

        webTestClient.post()
                .uri("/save")
                .body(fromObject(bankDTO))
                .header("Authorization", TestUtils.getAuthHeader("user1", "user1"))
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(Boolean.class)
                .value(Assert::assertTrue);

        assertEquals(1, bankRepository.findAll().size());
        assertEquals(1, cardRepository.findAll().size());
    }

    @Test
    public void testUnathorizedDetail() {
        webTestClient.get()
                .uri("/detail/1")
                .exchange()
                .expectStatus()
                .isUnauthorized();
    }

    @Test
    public void testDetail() {
        Bank bank = TestUtils.createBank();
        bank.setUserId("user1");
        bankRepository.save(bank);

        webTestClient.get()
                .uri("/detail/" + bank.getAppId())
                .header("Authorization", TestUtils.getAuthHeader("user1", "user1"))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(BankDTO.class)
                .value(foundBankDTO -> {
                    assertNotNull(foundBankDTO);
                    assertEquals(bank.getName(), foundBankDTO.getName());
                    assertEquals(bank.getUserId(), foundBankDTO.getUserId());
                    assertEquals(bank.getAppId(), foundBankDTO.getAppId());
                });
    }

    @Test
    public void testDeleteUnauthorized() {
        webTestClient.delete()
                .uri("/remove/1")
                .exchange()
                .expectStatus()
                .isUnauthorized();
    }

    @Test
    public void testDelete() {
        Bank bank = TestUtils.createBank();
        bank.setUserId("user1");
        bankRepository.save(bank);

        webTestClient.delete()
                .uri("/remove/" + bank.getAppId())
                .header("Authorization", TestUtils.getAuthHeader("user1", "user1"))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Boolean.class)
                .value(Assert::assertTrue);

        assertEquals(0, bankRepository.findAll().size());
    }
}