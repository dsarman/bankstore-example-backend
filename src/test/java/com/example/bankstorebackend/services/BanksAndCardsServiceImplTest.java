package com.example.bankstorebackend.services;

import com.example.bankstorebackend.TestUtils;
import com.example.bankstorebackend.data.dto.BankDTO;
import com.example.bankstorebackend.data.dto.CardDTO;
import com.example.bankstorebackend.data.model.Bank;
import com.example.bankstorebackend.data.model.Card;
import com.example.bankstorebackend.data.repositories.BankRepository;
import com.example.bankstorebackend.data.repositories.CardRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.test.StepVerifier;

import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BanksAndCardsServiceImplTest {

    @Autowired
    private BankRepository bankRepository;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private ReactiveRedisOperations<String, BankDTO> bankOps;

    @Autowired
    private BanksAndCardsService banksAndCardsService;

    @Before
    public void setUp() throws Exception {
        TestUtils.cleanAllPersistedData(cardRepository, bankRepository, bankOps);
    }

    @Test
    public void findByUserIdFromDB() {
        Bank bank = TestUtils.createBank();
        bankRepository.save(bank);

        StepVerifier.create(banksAndCardsService.findByUserId(bank.getUserId()))
                .assertNext(result -> {
                    assertEquals(bank.getAppId(), result.getAppId());
                    assertEquals(bank.getUserId(), result.getUserId());
                    assertEquals(bank.getName(), result.getName());
                })
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    public void findByUserIdFromRedis() {
        BankDTO bank = TestUtils.createBankDTO();
        String bankKey = bank.getUserId() + "-" + bank.getAppId();
        Boolean success = bankOps.opsForValue().set(bankKey, bank).block();
        assertEquals(true, success);

        StepVerifier.create(banksAndCardsService.findByUserId(bank.getUserId()))
                .assertNext(result -> {
                    assertEquals(bank.getAppId(), result.getAppId());
                    assertEquals(bank.getUserId(), result.getUserId());
                    assertEquals(bank.getName(), result.getName());
                })
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    public void saveWithCards() {
        BankDTO bankDTO = TestUtils.createBankDTO();
        IntStream.range(0, 10).forEach(i -> {
            bankDTO.getCards().add(TestUtils.createCardDTO());
        });

        StepVerifier.create(banksAndCardsService.save(bankDTO))
                .assertNext(result -> {
                    assertEquals(true, result);
                })
                .expectNextCount(0)
                .verifyComplete();


        List<Card> cards = cardRepository.findAll();
        assertEquals(10, cards.size());

        assertEquals(1, bankRepository.findAll().size());
        List<Bank> banks = bankRepository.findByUserId(bankDTO.getUserId());
        assertEquals(1, banks.size());
        assertEquals(bankDTO.getAppId(), banks.get(0).getAppId());
        assertEquals(bankDTO.getUserId(), banks.get(0).getUserId());
        assertEquals(bankDTO.getName(), banks.get(0).getName());
    }

    @Test
    public void findBankByUserIdAndAppIdFromRedis() {
        BankDTO bankDTO = TestUtils.createBankDTO();
        bankOps.opsForValue().set(bankDTO.getUserId() + "-" + bankDTO.getAppId(), bankDTO).block();

        StepVerifier.create(banksAndCardsService.findByUserIdAndBankAppId(bankDTO.getUserId(), bankDTO.getAppId()))
                .assertNext(foundBankDTO -> assertEquals(bankDTO, foundBankDTO))
                .verifyComplete();
    }

    @Test
    public void findBankByUserIdAndAppIdFromDB() {
        Bank bank = TestUtils.createBank();
        bankRepository.save(bank);
        StepVerifier.create(banksAndCardsService.findByUserIdAndBankAppId(bank.getUserId(), bank.getAppId()))
                .assertNext(foundBankDTO -> {
                    assertEquals(bank.getName(), foundBankDTO.getName());
                    assertEquals(bank.getUserId(), foundBankDTO.getUserId());
                    assertEquals(bank.getAppId(), foundBankDTO.getAppId());
                })
                .verifyComplete();
    }

    @Test
    public void deleteBankByUserIdAndAppId() {
        CardDTO cardDTO = TestUtils.createCardDTO();
        BankDTO bankDTO = TestUtils.createBankDTO();
        bankDTO.getCards().add(cardDTO);
        banksAndCardsService.save(bankDTO).block();

        assertEquals(1, cardRepository.findAll().size());
        assertEquals(1, bankRepository.findAll().size());

        // Set up complete, proceeding to test the actual deletion.

        StepVerifier.create(banksAndCardsService.deleteByUserIdAndBankAppId(bankDTO.getUserId(), bankDTO.getAppId()))
                .assertNext(Assert::assertTrue)
                .verifyComplete();

        StepVerifier.create(bankOps.opsForValue().get(bankDTO.getUserId() + "-" + bankDTO.getAppId()))
                .verifyComplete();

        assertEquals(0, cardRepository.findAll().size());
        assertEquals(0, bankRepository.findAll().size());
    }

}